package kai.dreams.wordclock;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.app.FragmentTransaction;

public class SettingsActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		FragmentTransaction tx = getFragmentManager().beginTransaction();
		tx.replace(android.R.id.content, new MyPreferenceFragment());
		tx.commit();
	}
	
	public static class MyPreferenceFragment extends PreferenceFragment{
		@Override
		public void onCreate(Bundle bundle) {
			super.onCreate(bundle);
			addPreferencesFromResource(R.xml.settings);
		}
	}

}
