var clock, hours, minutes, to, past, nbsps;
var indicators;
var chars = 'abcdefghijklmnopqrstuvwxyz';

var changed = function() {
  var now = new Date();
  var hour = now.getHours();
  var minute = now.getMinutes();
  var minute_frag = minute % 5;
  var minute_block = minute - minute_frag;
  var offset;
  var next;

  hour = hour % 12;

  for (var i in minutes) {
    minutes[i].removeClass('on');
  }
  for (var i in hours) {
    hours[i].removeClass('on');
  }

  to.removeClass('on');
  past.removeClass('on');


  if (minute_block > 30) {
    hours[(hour + 1) % 12].addClass('on');
    to.addClass('on');

  } else {
    hours[hour].addClass('on');
    if (minute !== 0) {
      past.addClass('on');
    }
  }

  offset = (minute > 30)?(60-minute_block):minute_block;

  if (offset in minutes) {
    minutes[offset].addClass('on');
  } else if (offset === 25) {
    minutes[20].addClass('on');
    minutes[5].addClass('on');
  }

  //Minutes indicator.
  console.log(minute_frag);
  for (var i in indicators) {
    if (minute_frag >= i) {
      indicators[i].addClass('on');
    } else {
      indicators[i].removeClass('on');
    }
  }


  now.setTime(Date.now());
  next = new Date(now.getTime());
  next.setMinutes(minute + 1);
  next.setSeconds(0);
  next.setMilliseconds(0);
  setTimeout(changed, next - now);

}

var first = function() {
  clock.css('margin-top',
      ($(window).height() - clock.outerHeight()) / 2 + 'px')
    .css('margin-left',
        ($(window).width() - clock.outerWidth()) / 2 + 'px');

  nbsps.each(function() {
    var c = chars.charAt(Math.floor(Math.random() * chars.length));
    $(this).text(c);
  });

  setTimeout(function() {
    progress.fadeIn();
  }, 1000);
};

var moveClock = function() {
  var maxMarginLeft = $(window).width() - clock.outerWidth();
  var maxMarginTop = $(window).height() - clock.outerHeight();
  var marginTop = Math.random() * maxMarginTop;
  var marginLeft = Math.random() * maxMarginLeft;

  clock.css('margin-top', marginTop + 'px');
  clock.css('margin-left', marginLeft + 'px');
};

$(function() {
  clock = $('#clock');

  minutes = {
    0: $('#clock #m_0'),
  5: $('#clock #m_5'),
  10: $('#clock #m_10'),
  15: $('#clock #m_15'),
  20: $('#clock #m_20'),
  30: $('#clock #m_30'),
  };

  hours = {
    0: $('#clock #h_0'),
  1: $('#clock #h_1'),
  2: $('#clock #h_2'),
  3: $('#clock #h_3'),
  4: $('#clock #h_4'),
  5: $('#clock #h_5'),
  6: $('#clock #h_6'),
  7: $('#clock #h_7'),
  8: $('#clock #h_8'),
  9: $('#clock #h_9'),
  10: $('#clock #h_10'),
  11: $('#clock #h_11'),
  };

  indicators = {
    1: $('#clock #indi_1'),
    2: $('#clock #indi_2'),
    3: $('#clock #indi_3'),
    4: $('#clock #indi_4'),
  };

  to = $('#clock #to');
  past = $('#clock #past');

  nbsps = $('#clock .nbsp');

  progress = $('#progressbar');

  $(document.body).on('click', moveClock);
  $(window).on('resize', moveClock);
  setInterval(moveClock, 60 * 1000);

  changed();

  first();
});
